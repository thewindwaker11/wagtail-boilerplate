#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project.core.settings.dev")
    this_dir = os.path.dirname(__file__)
    sys.path.insert(0, os.path.abspath(os.path.join(this_dir, 'project')))

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

